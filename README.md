# OMSeguros v1.0
#### Started on 08/10/2019

### Project config
##### Clone proyect

### How to run the project
#### Locally 
    `$ cd OMSeguros`
    `$docker-compose -f dev-docker-compose.yml up --build`

The project should be running on http://localhost:3000/<br>

To test the API you should use **Port 4000**<br>

### Git branching
- Always **create a new branch** under Develop or any other sub branch whenever you will start working on a task. 
- Before creating the branch you will be working on you should pull from the higher branches to reduce the number of conflicts that could come up in a merge.
- **Delete a branch** after it has been pushed to production.

#### TODO
Pull request process

## Server

## Filestructure
|____
| |____test-docker-compose.yml
| |____.DS_Store
| |____dev-docker-compose.yml
| |____server
| | |____Dockerfile-dev
| | |____yarn.lock
| | |____.dockerignore
| | |____package-lock.json
| | |____package.json
| | |____src
| | | |____server.js
| | | |____models
| | | | |____TestForm.js
| | | |____routes
| | | | |____exampleRoutes.js
| |____README.md
| |____.gitignore
| |____.env
| |____docker-compose.yml
| |____.git
| |____client
| | |____.DS_Store
| | |____Dockerfile-dev
| | |____README.md
| | |____.dockerignore
| | |____public
| | | |____index.html
| | | |____.DS_Store
| | | |____static
| | | | |____.DS_Store
| | | | |____css
| | | | | |____.DS_Store
| | | | | |____portero.css
| | | | |____img
| | | | | |____favicon.ico
| | | | | |____.DS_Store
| | | | | |____logo_no_background.png
| | | | | |____logo_main.png
| | | | |____icon
| | | | | |____.DS_Store
| | | | | |____envelope.svg
| | | | | |____notification.svg
| | | |____manifest.json
| | |____package-lock.json
| | |____package.json
| | |____src
| | | |____.DS_Store
| | | |____index.js
| | | |____components
| | | | |____Tabs
| | | | | |____Tabs.test.js
| | | | | |____Tabs.js
| | | | |____SideBar
| | | | | |____.DS_Store
| | | | | |____SideBar.js
| | | | | |____SideBar.test.js
| | | | |____.DS_Store
| | | | |____App
| | | | | |____App.test.js
| | | | | |____App.js
| | | | |____Dashboard
| | | | | |____Dashboard.test.js
| | | | | |____Dashboard.js
| | | | |____NewVisitorForm
| | | | | |____NewVisitorForm.js
| | | | |____Modal
| | | | | |____Modal.test.js
| | | | | |____Modal.js
| | | | |____MainContent
| | | | | |____MainContent.js
| | | | | |____.DS_Store
| | | | | |____MainContent.test.js
| | | | |____Header
| | | | | |____.DS_Store
| | | | | |____Header.js
| | | | | |____Header.test.js
| | | | |____DataTable
| | | | | |____DataTable.js
| | | | |____IncidentForm
| | | | | |____IncidentForm.js
| | | |____serviceWorker.js
